"""
This script creates shuffled networks and looks for significant differences between
the original and the shuffled. Running this for all values of o 
will give you table 3
"""
import networkx as nx
import matplotlib.pyplot as plt
import os
import numpy as np
from sklearn import preprocessing
from tools import *
from scipy.stats import mannwhitneyu, pointbiserialr, pearsonr
import statsmodels.api as sm
import collections
import pandas as pd

def get_degree_distribution(graphs):
    degrees = []
    for G in graphs:
        d = list(dict(G.degree).values())
        degrees += d

    return degrees

measure_names = ["$<N>$", "$<E>$", "ED", "SL", "$<CC>$", "D", "H", "NC", "$<AV>$", "$\\alpha$", "$x_{\min}$",  "A", "$k_{nn} \\alpha$"]

# Edit these to point to the transcript locations
control_transcripts_path = "ADReSS-IS2020-data/train/transcription/cc/"
ad_transcripts_path = "ADReSS-IS2020-data/train/transcription/cd/"

# Edit this to change the co-occurrence window
o = 5

control_graphs = load_all_files(control_transcripts_path, occurrence_window=o)
ad_graphs = load_all_files(ad_transcripts_path, occurrence_window=o)
random_control_graphs = load_shuffled_files(control_transcripts_path, occurrence_window=o, )
random_ad_graphs = load_shuffled_files(ad_transcripts_path, occurrence_window=o)

control_labels = [x.graph['ID'] for x in control_graphs]
ad_labels = [x.graph['ID'] for x in ad_graphs]

labels = np.array(control_labels + ad_labels)

# Edit this to point to the labels and meta data
meta_data = read_meta_data("ADReSS-IS2020-data/train/cc_meta_data.txt", dict())
meta_data = read_meta_data("ADReSS-IS2020-data/train/cd_meta_data.txt", meta_data)

control_X = vectorize_group(control_graphs)
ad_X = vectorize_group(ad_graphs)

X =  np.concatenate((control_X, ad_X))
y = np.ones(X.shape[0])
num_control = control_X.shape[0]
num_ad = ad_X.shape[0]
y[0:num_control] = 0

X_mmse, y_mmse = build_mmse_dataset(labels, meta_data, X)

random_control_X = np.zeros(control_X.shape)
random_ad_X = np.zeros(ad_X.shape)

for i in range(num_control):
    temp_X = vectorize_group(random_control_graphs[i])
    random_control_X[i, :] = temp_X.mean(axis=0)

for i in range(num_ad):
    temp_X = vectorize_group(random_ad_graphs[i])
    random_ad_X[i, :] = temp_X.mean(axis=0)
    
random_X = np.concatenate((random_control_X, random_ad_X))

control_labels = [x.graph['ID'] for x in control_graphs]
ad_labels = [x.graph['ID'] for x in ad_graphs]

pvals_random_control = np.zeros(control_X.shape[0])
pvals_random_ad = np.zeros(control_X.shape[0])

for x in range(control_X.shape[1]):
    try:
        _, pvals_random_control[x] = mannwhitneyu(control_X[:, x], random_control_X[:, x])
        _, pvals_random_ad[x] = mannwhitneyu(ad_X[:, x], random_ad_X[:, x])
    except ValueError as e:
        print(e)

txt = ""

for i in range(control_X.shape[1]):
    if pvals_random_control[i] < 0.05:
        txt += "%s & \\textbf{%.3f} & \\textbf{%.3f} " % (measure_names[i], control_X[:, i].mean(), random_control_X[:, i].mean())
    else:
        txt += "%s & %.3f & %.3f " % (measure_names[i], control_X[:, i].mean(), random_control_X[:, i].mean())
    if pvals_random_ad[i] < 0.05:
        txt += "& \\textbf{%.3f} & \\textbf{%.3f} " % (ad_X[:, i].mean(), random_ad_X[:, i].mean())
    else:
        txt += "& %.3f & %.3f " % (ad_X[:, i].mean(), random_ad_X[:, i].mean())

    txt += "\\\\\n"

print(txt)