"""
Runs the classification and regression tasks for the paper
"""
import matplotlib.pyplot as plt
import os
import numpy as np
from sklearn.linear_model import LogisticRegression, LinearRegression, RidgeCV, RidgeClassifier, Ridge
from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import accuracy_score, mean_squared_error, confusion_matrix
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.svm import SVC, LinearSVC, LinearSVR, SVR
from tools import *
from scipy.stats import pearsonr, spearmanr
from karateclub.graph_embedding import SF
import collections
import matplotlib
from sklearn.preprocessing import StandardScaler
import itertools
import pickle

# Set font size
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 16}

matplotlib.rc('font', **font)

def classifier_significantly_different(loo_res, loo_res_2):
    diff = np.sum(np.array(loo_res) - np.array(loo_res_2))
    if diff == 0:
        return 1
    return scipy.stats.wilcoxon(loo_res, loo_res_2, zero_method="pratt")[1]

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap, vmin=0, vmax=24)
    #plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def plot_bar_chart(lr_vals, rf_vals, lsvm_vals, rsvm_vals, labels, mmse=False):
    plt.figure()
    ind = np.arange(len(lr_vals)) 
    width = 0.2    
    plt.bar(ind, lr_vals, width, label='LR')
    plt.bar(ind + width, rf_vals, width, label='RF')
    plt.bar(ind + 2*width, lsvm_vals, width, label='LSVM')
    plt.bar(ind + 3*width, rsvm_vals, width, label='RSVM')

    ax = plt.gca()
    plt.xticks([0.1+r + width for r in range(len(lr_vals))], labels)
    plt.legend(loc='lower right')
    if mmse:
        plt.ylim([0, 9])
        plt.ylabel("RMSE")
    else:
        plt.ylim([0, 0.8])
        plt.ylabel("Accuracy")
    plt.tight_layout()

def plot_bar_chart_mmse(lr_vals, rf_vals, lsvm_vals, rsvm_vals, labels, mmse=True):
    plt.figure()
    ind = np.arange(len(lr_vals)) 
    width = 0.2  
    
    f, (ax, ax2) = plt.subplots(2, 1, sharex = True) # make the axes
    ax.bar(ind, lr_vals, width, label='LR')
    ax.bar(ind + width, rf_vals, width, label='RF')
    ax.bar(ind + 2*width, lsvm_vals, width, label='LSVM')
    ax.bar(ind + 3*width, rsvm_vals, width, label='RSVM')

    ax2.bar(ind, lr_vals, width, label='LR')
    ax2.bar(ind + width, rf_vals, width, label='RF')
    ax2.bar(ind + 2*width, lsvm_vals, width, label='LSVM')
    ax2.bar(ind + 3*width, rsvm_vals, width, label='RSVM')

    ax.set_ylim([20,50]) # numbers here are specific to this example
    ax2.set_ylim([0, 8]) # numbers here are specific to this example

    ax.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    #ax.xaxis.tick_bottom() 
    ax.tick_params(labeltop=False)
    ax2.xaxis.tick_bottom()
  
    ax2.set_xticks([0.1+r + width for r in range(len(lr_vals))])
    ax2.set_xticklabels(labels)
    #plt.legend(loc='lower right')
    #plt.ylim([0, 9])
    plt.ylabel("RMSE")

    #plt.tight_layout()

def run_test_set_classification(X, y, X_test, y_test, method=""):

    ss = StandardScaler()
    X = ss.fit_transform(X)
    X_test = ss.transform(X_test)
    logistic_clf = LogisticRegression(max_iter=1000)
    logistic_clf.fit(X, y)
    y_pred = logistic_clf.predict(X_test)
    logistic_accuracy = accuracy_score(y_test, y_pred)
    scores = logistic_clf.predict_proba(X_test)[:, 0]
    labels_ts = np.array(['AD', 'Control'])
    plt.figure()
    confusion_matrix_lr = confusion_matrix(y_test, y_pred)
    plot_confusion_matrix(confusion_matrix_lr, labels_ts)
    plt.tight_layout()
    plt.savefig("logistic_clf_%s_%s.png" % (o, method))

    random_clf = RandomForestClassifier()
    random_clf.fit(X, y)
    y_pred = random_clf.predict(X_test)
    rf_accuracy = accuracy_score(y_test, y_pred)
    plt.figure()
    confusion_matrix_rf = confusion_matrix(y_test, y_pred)
    plot_confusion_matrix(confusion_matrix_rf, labels_ts)
    plt.tight_layout()
    plt.savefig("rf_clf_%s_%s.png" % (o, method))

    svm_clf = LinearSVC(dual=False)
    svm_clf.fit(X, y)
    y_pred = svm_clf.predict(X_test)
    lsvm_accuracy = accuracy_score(y_test, y_pred)
    confusion_matrix_lsvm = confusion_matrix(y_test, y_pred)
    plot_confusion_matrix(confusion_matrix_lsvm, labels_ts)
    plt.tight_layout()
    plt.savefig("lsvm_clf_%s_%s.png" % (o, method))

    svm_clf = SVC()
    svm_clf.fit(X, y)
    y_pred = svm_clf.predict(X_test)
    rsvm_accuracy = accuracy_score(y_test, y_pred)
    confusion_matrix_rsvm = confusion_matrix(y_test, y_pred)
    plot_confusion_matrix(confusion_matrix_rsvm, labels_ts)
    plt.tight_layout()
    plt.savefig("rsvm_clf_%s_%s.png" % (o, method))

    return (method, logistic_accuracy, rf_accuracy, lsvm_accuracy, rsvm_accuracy, scores)

def run_test_set_regression(X_mmse, y_mmse, X_test, y_test_mmse, method="", test_y=None):
    ss = StandardScaler()
    X_mmse = ss.fit_transform(X_mmse)
    X_test = ss.transform(X_test)

    linear_pred = Ridge(alpha=0.5)
    linear_pred.fit(X_mmse, y_mmse)
    y_pred = linear_pred.predict(X_test)
    linear_scores_test = np.sqrt(mean_squared_error(y_test_mmse, y_pred))
    #linear_correlation = np.corrcoef(y_pred, y_test_mmse)[0, 1]
    linear_correlation = accuracy_score(y_pred < 24, test_y)

    linear_pred = LinearSVR(dual=False, loss='squared_epsilon_insensitive', max_iter=1000)
    linear_pred.fit(X_mmse, y_mmse)
    y_pred = linear_pred.predict(X_test)
    lsvm_scores_test = np.sqrt(mean_squared_error(y_test_mmse, y_pred))
    #lsvm_correlation = np.corrcoef(y_pred, y_test_mmse)[0, 1]
    lsvm_correlation = accuracy_score(y_pred < 24, test_y)

    linear_pred = SVR()
    linear_pred.fit(X_mmse, y_mmse)
    y_pred = linear_pred.predict(X_test)
    rsvm_scores_test = np.sqrt(mean_squared_error(y_test_mmse, y_pred))
    #rsvm_correlation = np.corrcoef(y_pred, y_test_mmse)[0, 1]
    rsvm_correlation = accuracy_score(y_pred < 24, test_y)

    rf_pred = RandomForestRegressor()
    rf_pred.fit(X_mmse, y_mmse)
    y_pred = rf_pred.predict(X_test)
    rf_scores_test = np.sqrt(mean_squared_error(y_test_mmse, y_pred))
    #rf_correlation = np.corrcoef(y_pred, y_test_mmse)[0, 1]
    rf_correlation = accuracy_score(y_pred < 24, test_y)

    return (method, linear_scores_test, rf_scores_test, lsvm_scores_test, rsvm_scores_test, linear_correlation, rf_correlation, lsvm_correlation, rf_correlation)


#@ignore_warnings(category=ConvergenceWarning)
def estimate_mmse(X, y, method=""):
    loo = LeaveOneOut()
    linear_scores = []
    rf_scores = []
    svm_scores = []
    svm_svc_scores = [] 

    for train_index, test_index in loo.split(X, y):
        X_train, X_test = X[train_index, :], X[test_index, :]
        y_train, y_test = y[train_index], y[test_index]

        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)
        
        linear_pred = Ridge(alpha=0.5)
        linear_pred.fit(X_train, y_train)
        y_pred = linear_pred.predict(X_test)
        linear_scores.append(np.sqrt(mean_squared_error(y_test, y_pred)))

        linear_pred = LinearSVR(dual=False, loss='squared_epsilon_insensitive', max_iter=1000)
        linear_pred.fit(X_train, y_train)
        y_pred = linear_pred.predict(X_test)
        svm_scores.append(np.sqrt(mean_squared_error(y_test, y_pred)))

        linear_pred = SVR()
        linear_pred.fit(X_train, y_train)
        y_pred = linear_pred.predict(X_test)
        svm_svc_scores.append(np.sqrt(mean_squared_error(y_test, y_pred)))

        rf_pred = RandomForestRegressor()
        rf_pred.fit(X_train, y_train)
        y_pred = rf_pred.predict(X_test)
        rf_scores.append(np.sqrt(mean_squared_error(y_test, y_pred)))

    s = (method, np.mean(linear_scores), np.mean(rf_scores), np.mean(svm_scores), np.mean(svm_svc_scores))
    return s

def match_classification_and_mmse(mmse_dct, classification_dct, y_true, labels):
    mmse = []
    classification = []
    true_labels = []
    for val in mmse_dct:
        try:
            mmse.append(mmse_dct[val])
        except KeyError as e:
            pass

        try:
            classification.append(classification_dct[val][0])
        except KeyError as e:
            mmse = mmse[:-1]

        true_labels.append(y_true[labels == val][0])
    return mmse, classification, true_labels

def turn_loo_results_list_into_string(vals, classification=True):
    """
    Calculates which result is the best to make it italic and turns the list
    into a nice latex table
    """
    if classification:
        max_val = -1
    else:
        max_val = np.inf
    for row in vals:
        if classification:
            max_val = max(max(row[1:]), max_val)
        else:
            max_val = min(min(row[1:]), max_val)

    s = ""
    for row in vals:
        s += row[0] + " , "
        for v in row[1:]:
            if v == max_val:
                s += "%.3f , " % v
                #s += "\\textit{%s} & " % v

            else:
                s += "%.3f, " % v
                #s += "%s & " % v
        s = s[:-2]
        s += "\n"
    s += "\n"
    return s

def turn_loo_results_list_into_string_csv(vals, classification=True):
    """
    Calculates which result is the best to make it italic and turns the list
    into a nice latex table
    """
    if classification:
        max_val = -1
    else:
        max_val = np.inf
    for row in vals:
        if classification:
            max_val = max(max(row[1:]), max_val)
        else:
            max_val = min(min(row[1:]), max_val)

    s = ""
    for row in vals:
        s += row[0] + " , "
        for v in row[1:]:
            if v == max_val:
                s += "%.3f , " % v
                #s += "\\textit{%s} & " % v

            else:
                s += "%.3f, " % v
                #s += "%s & " % v
        s = s[:-2]
        s += "\n"
    s += "\n"
    return s

#@ignore_warnings(category=ConvergenceWarning)
def run_loo(X, y, method="", labels = []):
    loo = LeaveOneOut() 
    logistic_scores = []
    embed_scores = []
    svm_scores = []
    svm_svc_scores = [] 
    dummy_scores = [] 

    classification_logistic = {}
    classification_rf = {}
    classification_lsvm = {}
    classification_rsvm = {}
    y_pred_out = np.zeros(y.shape)
    for train_index, test_index in loo.split(X, y):
        X_train, X_test = X[train_index, :], X[test_index, :]
        y_train, y_test = y[train_index], y[test_index]

        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)
        label = labels[test_index]

        logistic_clf = LogisticRegression(max_iter=1000)
        logistic_clf.fit(X_train, y_train)
        y_pred = logistic_clf.predict(X_test)
        logistic_scores.append(accuracy_score(y_test, y_pred))

        random_clf = RandomForestClassifier()
        random_clf.fit(X_train, y_train)
        y_pred = random_clf.predict(X_test)
        y_pred_out[test_index] = y_pred
        embed_scores.append(accuracy_score(y_test, y_pred))

        classification_logistic[label[0]] = y_pred
        svm_clf = LinearSVC(dual=False)
        svm_clf.fit(X_train, y_train)
        y_pred = svm_clf.predict(X_test)
        svm_scores.append(accuracy_score(y_test, y_pred))

        svm_clf = SVC()
        svm_clf.fit(X_train, y_train)
        y_pred = svm_clf.predict(X_test)
        svm_svc_scores.append(accuracy_score(y_test, y_pred))

        #dummy_clf = DummyClassifier(strategy='uniform')
        #dummy_clf.fit(X_train, y_train)
        #y_pred = dummy_clf.predict(X_test)
        #dummy_scores.append(accuracy_score(y_test, y_pred))
    s = (method, np.mean(logistic_scores), np.mean(embed_scores), np.mean(svm_scores), np.mean(svm_svc_scores)) #np.mean(dummy_scores))
    vals = (logistic_scores, embed_scores, svm_scores, svm_svc_scores)
    return classification_logistic, vals, s

def save_obj(obj, name ):
    with open('obj_'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

np.random.seed(0)

measure_names = ["$N$", "$E$", "ED", "SL", "$<CC>$", "D", "H", "NC", "$<AV>$", "$\\alpha$", "$x_{\min}$",  "A", "$k_{nn} \\alpha$"]

# Edit these to point to the training set transcipt locations
control_transcripts_path = "ADReSS-IS2020-data/train/transcription/cc/"
ad_transcripts_path = "ADReSS-IS2020-data/train/transcription/cd/"

# Edit this to set the co-occurrence window
o = 3

control_graphs = load_all_files(control_transcripts_path, occurrence_window=o)
ad_graphs = load_all_files(ad_transcripts_path, occurrence_window=o)

# Edit these to point to the labels
meta_data = read_meta_data("ADReSS-IS2020-data/train/cc_meta_data.txt", dict())
meta_data = read_meta_data("ADReSS-IS2020-data/train/cd_meta_data.txt", meta_data)

# Edit this to point to the test set
test_set_path = "ADReSS-IS2020-data/test/transcription/"

test_graphs = load_all_files(test_set_path, occurrence_window=o)
ad_dct, mmse_dct = read_meta_test_data("ADReSS-IS2020-data/test/meta_data_test.txt")

unigram_control_X = calculate_unigram_stats(control_transcripts_path)
unigram_ad_X = calculate_unigram_stats(ad_transcripts_path)

X_unigram = np.concatenate((unigram_control_X, unigram_ad_X))
X_test_unigram = calculate_unigram_stats(test_set_path)

control_X = vectorize_group(control_graphs)
ad_X = vectorize_group(ad_graphs)
test_X = vectorize_group(test_graphs)
test_y = match_ad_dct_with_dataset(ad_dct, test_graphs)

control_labels = [x.graph['ID'] for x in control_graphs]
ad_labels = [x.graph['ID'] for x in ad_graphs]
test_labels = [x.graph['ID'] for x in test_graphs]

labels = np.array(control_labels + ad_labels)

num_control = control_X.shape[0]
num_ad = ad_X.shape[0]
X =  np.concatenate((control_X, ad_X))
y = np.ones(X.shape[0])

y[0:num_control] = 0

classification_results = []
regression_results = []
classification_results_test = []
regression_results_test = []
regression_correlation_test = []
classification_s = ""
regression_s = ""
total_results = []
classifier_result = collections.defaultdict(list)

lr_vals = []
rf_vals = []
lsvm_vals = []
rsvm_vals = []

lr_mmse_vals = []
rf_mmse_vals = []
lsvm_mmse_vals = []
rsvm_mmse_vals = []

lr_test_vals = []
rf_test_vals = []
lsvm_test_vals = []
rsvm_test_vals = []

lr_test_mmse_vals = []
rf_test_mmse_vals = []
lsvm_test_mmse_vals = []
rsvm_test_mmse_vals = []

classification_unigram, unigram_vals, s = run_loo(X_unigram, y, "Unigram ", labels)
classification_results.append(s)
save_obj(unigram_vals, "unigram_vals")

lr_vals += [s[1]]
rf_vals += [s[2]]
lsvm_vals += [s[3]]
rsvm_vals += [s[4]]

classification_logistic, gm_vals, s = run_loo(X, y, "Graph Measures ", labels)
classification_results.append(s)
total_results.append(s)
save_obj(gm_vals, "gm_vals_%s" % o)

classification_s += turn_loo_results_list_into_string_csv(classification_results)

lr_vals += [s[1]]
rf_vals += [s[2]]
lsvm_vals += [s[3]]
rsvm_vals += [s[4]]

lr_test_mmse_corr = []
rf_test_mmse_corr = []
lsvm_test_mmse_corr = []
rsvm_test_mmse_corr = []



# Compare unigram and gm
diff_significant = {}
for i,res in enumerate(unigram_vals):
    s, p = scipy.stats.wilcoxon(res, gm_vals[i], zero_method="pratt")
    diff_significant[i] = p

print("GM")
print(diff_significant)

X_mmse, y_mmse = build_mmse_dataset(labels, meta_data, X)

X_test_mmse, y_test_mmse = build_mmse_dataset(test_labels, mmse_dct, test_X)

X_unigram_mmse, y_mmse = build_mmse_dataset(labels, meta_data, X_unigram)

X_test_unigram_mmse, y_test_mmse = build_mmse_dataset(test_labels, mmse_dct, X_test_unigram)

s = estimate_mmse(X_unigram_mmse, y_mmse, "Unigram, ")
lr_mmse_vals += [s[1]]
rf_mmse_vals += [s[2]]
lsvm_mmse_vals += [s[3]]
rsvm_mmse_vals += [s[4]]
regression_results.append(s)

s = estimate_mmse(X_mmse, y_mmse, "Graph Measures, ")
lr_mmse_vals += [s[1]]
rf_mmse_vals += [s[2]]
lsvm_mmse_vals += [s[3]]
rsvm_mmse_vals += [s[4]]
regression_results.append(s)

regression_s += turn_loo_results_list_into_string_csv(regression_results, False)

s = run_test_set_classification(X_unigram, y, X_test_unigram, test_y, "Unigram, ")
classification_results_test.append(s[0:5])
lr_test_vals += [s[1]]
rf_test_vals += [s[2]]
lsvm_test_vals += [s[3]]
rsvm_test_vals += [s[4]]

s = run_test_set_classification(X, y, test_X, test_y, "Graph Measures, ")
lr_test_vals += [s[1]]
rf_test_vals += [s[2]]
lsvm_test_vals += [s[3]]
rsvm_test_vals += [s[4]]
scores = s[5]
classification_results_test.append(s[0:5])



s = run_test_set_regression(X_unigram_mmse, y_mmse, X_test_unigram_mmse, y_test_mmse, "Unigram, ", test_y)

regression_results_test.append(s[0:5])
regression_correlation_test.append((s[0],) + s[5:])

lr_test_mmse_vals += [s[1]]
rf_test_mmse_vals += [s[2]]
lsvm_test_mmse_vals += [s[3]]
rsvm_test_mmse_vals += [s[4]]
lr_test_mmse_corr += [s[5]]
rf_test_mmse_corr += [s[6]]
lsvm_test_mmse_corr += [s[7]]
rsvm_test_mmse_corr += [s[8]]

classification_test_s = turn_loo_results_list_into_string(classification_results_test)

s = run_test_set_regression(X_mmse, y_mmse, X_test_mmse, y_test_mmse, "Graph Measures, ", test_y)

regression_results_test.append(s[0:5])
regression_correlation_test.append((s[0],) + s[5:])

lr_test_mmse_vals += [s[1]]
rf_test_mmse_vals += [s[2]]
lsvm_test_mmse_vals += [s[3]]
rsvm_test_mmse_vals += [s[4]]
lr_test_mmse_corr += [s[5]]
rf_test_mmse_corr += [s[6]]
lsvm_test_mmse_corr += [s[7]]
rsvm_test_mmse_corr += [s[8]]

regression_test_s = turn_loo_results_list_into_string(regression_results_test, False)
correlation_test_s = turn_loo_results_list_into_string(regression_correlation_test, False)

classification_results = []
regression_results = []

classification_results_test = []
regression_results_test = []

regression_correlation_test = []

k_vals_lr = {}
k_vals_rf = {}
k_vals_lsvm = {}
k_vals_rsvm = {}

ks = [5, 10, 15, 20, 50]
for k in ks:
    control_embed_X = embed_group(control_graphs, k)
    ad_embed_X = embed_group(ad_graphs, k)
    test_embed_X = embed_group(test_graphs, k)

    X_embed =  np.concatenate((control_embed_X, ad_embed_X))
    _, vals, s = run_loo(X_embed, y, "SF , %s" % k, labels)
    k_vals_lr[k] = vals[0]
    k_vals_rf[k] = vals[1]
    k_vals_lsvm[k] = vals[2]
    k_vals_rsvm[k] = vals[3]

    # Compare unigram and gm
    diff_significant = {}
    for i,res in enumerate(unigram_vals):
        _, p = scipy.stats.wilcoxon(res, vals[i], zero_method="pratt")
        diff_significant[i] = p
    
    print("k = %s" % k)
    print(diff_significant)

    lr_vals += [s[1]]
    rf_vals += [s[2]]
    lsvm_vals += [s[3]]
    rsvm_vals += [s[4]]
    X_mmse, y_mmse = build_mmse_dataset(labels, meta_data, X_embed)
    classification_results.append(s)
    total_results.append(s)
    s = estimate_mmse(X_mmse, y_mmse, "SF , %s" % k)
    regression_results.append(s)    

    lr_mmse_vals += [s[1]]
    rf_mmse_vals += [s[2]]
    lsvm_mmse_vals += [s[3]]
    rsvm_mmse_vals += [s[4]]

    s = run_test_set_classification(X_embed, y, test_embed_X, test_y, "SF, %s" % k)
    classification_results_test.append(s[0:5])

    lr_test_vals += [s[1]]
    rf_test_vals += [s[2]]
    lsvm_test_vals += [s[3]]
    rsvm_test_vals += [s[4]]
    s = run_test_set_regression(X_mmse, y_mmse, test_embed_X, y_test_mmse, "SF, %s" % k, test_y)
    regression_results_test.append(s[0:5])

    lr_test_mmse_vals += [s[1]]
    rf_test_mmse_vals += [s[2]]
    lsvm_test_mmse_vals += [s[3]]
    rsvm_test_mmse_vals += [s[4]]

    lr_test_mmse_corr += [s[5]]
    rf_test_mmse_corr += [s[6]]
    lsvm_test_mmse_corr += [s[7]]
    rsvm_test_mmse_corr += [s[8]]
    regression_correlation_test.append((s[0],) + s[5:])

save_obj(k_vals_lr, "k_vals_lr_%s" % o)
save_obj(k_vals_rf, "k_vals_rf_%s" % o)
save_obj(k_vals_lsvm, "k_vals_lsvm_%s" % o)
save_obj(k_vals_rsvm, "k_vals_rvm_%s" % o)

# Compare the different ks

ks_significantly_different_lr = np.ones((len(ks), len(ks)))
ks_significantly_different_rf = np.ones((len(ks), len(ks)))
ks_significantly_different_lsvm = np.ones((len(ks), len(ks)))
ks_significantly_different_rsvm = np.ones((len(ks), len(ks)))

for i,k in enumerate(ks):
    for j,k_2 in enumerate(ks):
        if k == k_2:
            continue
        ks_significantly_different_lr[i, j] = classifier_significantly_different(k_vals_lr[k], k_vals_lr[k_2])
        ks_significantly_different_rf[i, j] = classifier_significantly_different(k_vals_rf[k], k_vals_rf[k_2])
        ks_significantly_different_lsvm[i, j] = classifier_significantly_different(k_vals_lsvm[k], k_vals_lsvm[k_2])
        ks_significantly_different_rsvm[i, j] = classifier_significantly_different(k_vals_rsvm[k], k_vals_rsvm[k_2])

print(np.count_nonzero(ks_significantly_different_lr < 0.05))
print(np.count_nonzero(ks_significantly_different_rf < 0.05))
print(np.count_nonzero(ks_significantly_different_lsvm < 0.05))
print(np.count_nonzero(ks_significantly_different_rsvm < 0.05))


labels = ["U", "GM", "k=5", "k=10", "k=15", "k=20", 'k=50']

plot_bar_chart(lr_vals, rf_vals, lsvm_vals, rsvm_vals, labels)
plt.savefig("train_classification_o=%s.png" % o)
plot_bar_chart(lr_mmse_vals, rf_mmse_vals, lsvm_mmse_vals, rsvm_mmse_vals, labels, True)
plt.savefig("train_regression_o=%s.png" % o)

plot_bar_chart(lr_test_vals, rf_test_vals, lsvm_test_vals, rsvm_test_vals, labels)
plt.savefig("test_classification_o=%s.png" % o)
plot_bar_chart(lr_test_mmse_vals, rf_test_mmse_vals, lsvm_test_mmse_vals, rsvm_test_mmse_vals, labels, True)
plt.savefig("test_regression_o=%s.png" % o)
plot_bar_chart(lr_test_mmse_corr, rf_test_mmse_corr, lsvm_test_mmse_corr, rsvm_test_mmse_corr, labels, False)
plt.savefig("test_regression_corr_o=%s.png" % o)

classification_s += turn_loo_results_list_into_string_csv(classification_results)
regression_s += turn_loo_results_list_into_string_csv(regression_results, False)
classification_test_s += turn_loo_results_list_into_string_csv(classification_results_test)
regression_test_s += turn_loo_results_list_into_string_csv(regression_results_test, False)
correlation_test_s += turn_loo_results_list_into_string_csv(regression_correlation_test, False)

print("Classification Train")
print()
print("Method, LR, RF, LSVM, RSVM \n")
print(classification_s)
print()
print("Regression Train")
print()
print("Method, LR, RF, LSVM, RSVM \n")
print(regression_s)
print()
print("Classification Test")
print()
print("Method, LR, RF, LSVM, RSVM \n")
print(classification_test_s)
print()
print("Regression Test")
print()
print("Method, LR, RF, LSVM, RSVM \n")
print(regression_test_s)
print()
print("Classification Using Regression")
print()
print("Method, LR, RF, LSVM, RSVM \n")
print(correlation_test_s)
print()

plt.close('all')