"""
This compares the differences between the LOO classifier results
for significance
"""
import pickle
import scipy.stats
import numpy as np

def load_obj(name ):
    with open('obj_' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def classifier_significantly_different(loo_res, loo_res_2):
    diff = np.sum(np.array(loo_res) - np.array(loo_res_2))
    if diff == 0:
        return 1
    return scipy.stats.wilcoxon(loo_res, loo_res_2, zero_method="pratt")[1]

ks = [5, 10, 15, 20, 50]
os = [2, 3, 5]

unigram_vals = load_obj("unigram_vals")

k_vals_lr = {}
k_vals_rf = {}
k_vals_lsvm = {}
k_vals_rsvm = {}
gm_vals = {}
for o in os:
    gm_vals[o] = load_obj("gm_vals_%s" % o)
    k_vals_lr[o] = load_obj("k_vals_lr_%s" % o)
    k_vals_rf[o] = load_obj("k_vals_rf_%s" % o)
    k_vals_lsvm[o] = load_obj("k_vals_lsvm_%s" % o)
    k_vals_rsvm[o] = load_obj("k_vals_rvm_%s" % o)

# Now we compare the values
gm_diff_lr = np.ones((len(os), len(os)))
gm_diff_rf = np.ones((len(os), len(os)))
gm_diff_lsvm = np.ones((len(os), len(os)))
gm_diff_rsvm = np.ones((len(os), len(os)))

sf_diff_lr = np.ones((len(ks), len(os), len(os)))
sf_diff_rf = np.ones((len(ks), len(os), len(os)))
sf_diff_lsvm = np.ones((len(ks), len(os), len(os)))
sf_diff_rsvm = np.ones((len(ks), len(os), len(os)))

for i,o in enumerate(os):
    for j,o_2 in enumerate(os):
        if o == o_2:
            continue
        gm_diff_lr[i, j] = classifier_significantly_different(gm_vals[o][0], gm_vals[o_2][0])
        gm_diff_rf[i, j] = classifier_significantly_different(gm_vals[o][1], gm_vals[o_2][1])
        gm_diff_lsvm[i, j] = classifier_significantly_different(gm_vals[o][2], gm_vals[o_2][2])
        gm_diff_rsvm[i, j] = classifier_significantly_different(gm_vals[o][3], gm_vals[o_2][3])

        for z,k in enumerate(ks):
            sf_diff_lr[z, i, j] = classifier_significantly_different(k_vals_lr[o][k], k_vals_lr[o_2][k])
            sf_diff_rf[z, i, j] = classifier_significantly_different(k_vals_rf[o][k], k_vals_rf[o_2][k])
            sf_diff_lsvm[z, i, j] = classifier_significantly_different(k_vals_lsvm[o][k], k_vals_lsvm[o_2][k])
            sf_diff_rsvm[z, i, j] = classifier_significantly_different(k_vals_rsvm[o][k], k_vals_rsvm[o_2][k])

print(gm_diff_lr)
print(gm_diff_rf)
print(gm_diff_lsvm) 
print(gm_diff_rsvm)
print()

print(np.count_nonzero(gm_diff_lr < 0.05))
print(np.count_nonzero(gm_diff_rf < 0.05))
print(np.count_nonzero(gm_diff_lsvm < 0.05))
print(np.count_nonzero(gm_diff_rsvm < 0.05))


for i,k in enumerate(ks):
    print(k)
    print(sf_diff_lr[i, :, :])
    print(sf_diff_rf[i, :, :])
    print(sf_diff_lsvm[i, :, :])
    print(sf_diff_rsvm[i, :, :])

    print(np.count_nonzero(sf_diff_lr[i, :, :] < 0.05))
    print(np.count_nonzero(sf_diff_rf[i, :, :] < 0.05))
    print(np.count_nonzero(sf_diff_lsvm[i, :, :] < 0.05))
    print(np.count_nonzero(sf_diff_rsvm[i, :, :] < 0.05))