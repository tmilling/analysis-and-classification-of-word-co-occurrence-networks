"""
This script calculates the various network measures and performs some statistical tests to 
look for significant differences. Run this to generate Tables 1 and 2, and to get
example networks (these will be saved as a graphml file)
"""
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from tools import *
from scipy.stats import mannwhitneyu, pearsonr
import collections
import pandas as pd

measure_names = ["$<N>$", "$<E>$", "ED", "SL", "$<CC>$", "D", "H", "NC", "$<AV>$", "$\\alpha$", "$x_{\min}$", "A", "$k_{nn} \\alpha$"]

# Edit these to point to the transcripts location
control_transcripts_path = "ADReSS-IS2020-data/train/transcription/cc/"
ad_transcripts_path = "ADReSS-IS2020-data/train/transcription/cd/"

os = [2, 3, 5]
num_o = len(os)
pvals = np.zeros((num_o, 13))
control_X = np.zeros((num_o, 54, 13))
ad_X = np.zeros((num_o, 54, 13))
correlation_df = dict()
p_values_df = dict()

for i,o in enumerate(os):
    temp_corr_df = pd.DataFrame()
    temp_pvalues_df = pd.DataFrame()
    control_graphs = load_all_files(control_transcripts_path, occurrence_window=o)
    ad_graphs = load_all_files(ad_transcripts_path, occurrence_window=o)

    nx.write_graphml(control_graphs[0], "control_graph_%s.graphml" % o)
    nx.write_graphml(ad_graphs[0], "ad_graph_%s.graphml" % o)

    meta_data = read_meta_data("ADReSS-IS2020-data/train/cc_meta_data.txt", dict())
    meta_data = read_meta_data("ADReSS-IS2020-data/train/cd_meta_data.txt", meta_data)

    control_X[i, :, :] = vectorize_group(control_graphs)
    ad_X[i, :, :] = vectorize_group(ad_graphs)
    X =  np.concatenate((control_X[i, :, :], ad_X[i, :, :]))
    control_labels = [x.graph['ID'] for x in control_graphs]
    ad_labels = [x.graph['ID'] for x in ad_graphs]
    y = np.zeros(X.shape[0])
    y[control_X.shape[1]:] = 1
    labels = np.array(control_labels + ad_labels)

    X_mmse, y_mmse, y_mmse_tags = build_mmse_dataset(labels, meta_data, X, y)

    for x in range(control_X.shape[2]):
        try:
            _, pvals[i, x] = mannwhitneyu(control_X[i, :, x], ad_X[i, :, x])
        except ValueError as e:
            print(e)

    for j in range(len(measure_names)):
        corr, p = spearmanr(X[1:, j], y_mmse)
        temp_corr_df[measure_names[j]] = pd.Series(corr) 
        temp_pvalues_df[measure_names[j]] = pd.Series(p)

    correlation_df[o] = temp_corr_df
    p_values_df[o] = temp_pvalues_df

txt = ""
for i in range(control_X.shape[2]):
    txt += "%s & " % measure_names[i] 
    for j in range(num_o):
        if pvals[j, i] < 0.05:
            txt += "\\textbf{%.3f} & \\textbf{%.3f} & " % (control_X[j, :, i].mean(), ad_X[j, :, i].mean())
        else:
            txt += "%.3f & %.3f & " % (control_X[j, :, i].mean(), ad_X[j, :, i].mean())

    txt = txt[:-2]
    txt += "\\\\\n"
print(txt)

txt = ""
for i in range(control_X.shape[2]):
    txt += "%s & " % measure_names[i] 
    for j,o in enumerate(os):
        if p_values_df[o].iloc[0][i] < 0.05:
            txt += "\\textbf{%.3f} & " % (correlation_df[o].iloc[0][i])
        else:
            txt += "%.3f & " % (correlation_df[o].iloc[0][i])
    txt = txt[:-2]
    txt += "\\\\\n"
print(txt)